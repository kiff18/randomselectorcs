using System;
using System.Collections.Generic;

namespace randomselectorcs
{
    class RandomSelector
    {
        protected List<string> players;
        
        public void SetPlayers()
        {
            players = new List<string>();
            int x = 0;
            Console.WriteLine("Insert amount of players.");
            
            if(int.TryParse(Console.ReadLine(), out x))
            {
                for(int i = 0; i < x; i++)
                {
                    Console.WriteLine("Insert player name.");
                    players.Add(Console.ReadLine());
                    
                }
                Console.WriteLine("Done.");
            }

        }

        public string GetPlayer()
        {
            if(players.Count > 0)
            {
                var rand = new Random();
                int x = rand.Next(players.Count);
                Console.WriteLine(players[x]);

            }
            else
            {
                Console.WriteLine("No players in list.");
            }
            return "";
        }


    }
}
