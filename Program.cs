﻿using System;

namespace randomselectorcs
{
    class Program
    {
        static void Main(string[] args)
        {
            int caseSwitch = 0;

            RandomSelector randomSelector = new RandomSelector();

            
            
            while(true)
            {
                Console.WriteLine("1. Input Characters.");
                Console.WriteLine("2. Pick Random Character.");
                
                if(int.TryParse(Console.ReadLine(), out caseSwitch))
                {
                    switch (caseSwitch)
                    {
                        case 1:
                            randomSelector.SetPlayers();
                            break;
                        case 2:
                            Console.WriteLine(randomSelector.GetPlayer());
                            break;
                        default:
                            Console.WriteLine("Default case");
                            break;
                    }
                }
            }

        }
    }
}
